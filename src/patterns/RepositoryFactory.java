package patterns;

import repositories.PedidoMongoRepository;
import repositories.PedidoMySQLRepository;
import repositories.PedidoRepository;

public class RepositoryFactory {

    public static PedidoRepository getPedidoRepository(){
        //return new PedidoMySQLRepository();
        return new PedidoMongoRepository();
    }

}
