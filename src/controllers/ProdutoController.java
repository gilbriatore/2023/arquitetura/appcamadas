package controllers;

import models.Produto;
import services.ProdutoService;

import java.util.List;

public class ProdutoController {

    ProdutoService service = new ProdutoService();
    public List<Produto> listar(String criterios) {
        return service.listar(criterios);
    }
}
