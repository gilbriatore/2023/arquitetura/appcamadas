package controllers;

import models.Cliente;
import services.UsuarioService;

public class UsuarioController {

    UsuarioService service = new UsuarioService();
    public Cliente login(String usuario, String senha) {
        return service.login(usuario, senha);
    }
}
