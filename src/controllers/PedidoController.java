package controllers;

import java.util.List;

import models.Cliente;
import models.Pedido;
import models.Produto;
import patterns.RepositoryFactory;
import repositories.PedidoRepository;
import services.PedidoService;
public class PedidoController {
    PedidoService service;
    public PedidoController(){
        PedidoRepository repository = RepositoryFactory.getPedidoRepository();
        this.service =  new PedidoService(repository);
    }
    public Pedido realizar(Cliente cliente, List<Produto> produtos) {
        return service.realizarPedido(cliente, produtos);
    }
}
