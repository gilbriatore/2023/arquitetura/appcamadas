package repositories;

import models.Pedido;

public interface PedidoRepository {
    Pedido salvar(Pedido pedido);
}
