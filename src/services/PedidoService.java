package services;

import java.util.List;

import models.Cliente;
import models.Pedido;
import models.Produto;
import repositories.PedidoRepository;

public class PedidoService {
    PedidoRepository repository;
    public PedidoService(PedidoRepository repository){
        this.repository = repository;
    }

    public Pedido realizarPedido(Cliente cliente, List<Produto> produtos) {
        Pedido pedido = new Pedido();
        pedido.cliente = cliente;
        pedido.produtos = produtos;
        return repository.salvar(pedido);
    }
}
