package services;

import models.Produto;
import repositories.ProdutoRepository;

import java.util.List;

public class ProdutoService {

    ProdutoRepository repository = new ProdutoRepository();
    public List<Produto> listar(String criterios) {
        return repository.listar(criterios);
    }
}
