package services;

import models.Cliente;
import repositories.UsuarioRepository;

public class UsuarioService {

    UsuarioRepository repository = new UsuarioRepository();
    public Cliente login(String usuario, String senha) {
        Cliente cliente = repository.login(usuario, senha);
        if(cliente != null){
            return cliente;
        }
        return null;
    }
}
