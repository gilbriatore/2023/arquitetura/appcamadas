import controllers.UsuarioController;
import controllers.PedidoController;
import controllers.ProdutoController;
import models.Pedido;
import models.Produto;
import models.Cliente;

import java.util.List;

public class View {
    public static void main(String[] args) {

        //Caso de uso: efetuar pedido
        // 1) Cliente autenticado
        UsuarioController usuarioCrtl = new UsuarioController();
        Cliente cliente = usuarioCrtl.login("usuario", "senha");

        // 2) Lista de produtos
        ProdutoController produtoCtrl = new ProdutoController();
        List<Produto> produtos = produtoCtrl.listar("criterios");

        // 3) Realizar o pedido
        PedidoController pedidoCtrl = new PedidoController();
        Pedido pedido = pedidoCtrl.realizar(cliente, produtos);

        // Pedido realizado com sucesso!

    }
}